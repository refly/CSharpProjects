﻿
using System;
using System.Collections.Generic;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace COM.CK
{
    class ComPortTransport
    {
        public static sjqh COMPORT_CONTROLLER = new sjqh();
        int receive_count = 0;
        /// <summary>
        /// 描述：com口操作层初始换
        /// </summary>
        /// <param name="comPortName">com口的名称</param>
        public void Initialize(String comPortName)
        {
            bool initResult = COMPORT_CONTROLLER.init_wms_sys(1, comPortName.ToUpper());
            if (!initResult)
                throw new Exception("端口初始化失败");
            COMPORT_CONTROLLER.check_state = true;
            COMPORT_CONTROLLER.on_dis_event += new sjqh.receive_dis_data(receive_dis_data);

            COMPORT_CONTROLLER.on_send_dis_result_event += new sjqh.send_dis_data_result(receive_dis_data_result);
            COMPORT_CONTROLLER.on_close_channel += new sjqh.close_channel(aa_on_close_channel);
            COMPORT_CONTROLLER.on_config_channel += new sjqh.config_channel(aa_on_config_channel);
            COMPORT_CONTROLLER.on_open_channel += new sjqh.open_channel(aa_on_open_channel);
            //7E 00 FF FF 34 67 7F 
            //7E 01 00 00 34 BB 7F
            model.dis_id id;
            id.order = 0;
            id.ele_id = 65535;
            id.state = 1;
            COMPORT_CONTROLLER.write_reset_device(id);
            System.Threading.Thread.Sleep(100);
            id.order = 1;
            id.ele_id = 0;
            id.state = 1;
            COMPORT_CONTROLLER.write_reset_device(id);
        }


        private void receive_dis_data(model.uart_dis_data[] data)
        {
            if (data.Length == 1)
            {
                Console.WriteLine("receive_dis_data"+data.ToString());
            }
            else
            {
                Console.WriteLine("receive_dis_data"+data.ToString());
            }
            Console.WriteLine(++receive_count);

        }
        private void receive_dis_data_result(byte order, byte result)
        {
            Console.WriteLine("receive_dis_data_result");
        }
        void aa_on_close_channel(model.close_channel_led data)
        {
            Console.WriteLine("关闭通道灯 id = " + data.channel_id.ToString() + " 成功");

        }
        void aa_on_config_channel(model.config_channel_led data)
        {
            Console.WriteLine("配置 id = " + data.channel_id.ToString() + "配置字 = " + data.config_word.ToString() + " 成功 ");
        }
        void aa_on_open_channel(model.open_channel_led data)
        {
            Console.WriteLine("打开通道灯 id = " + data.channel_id.ToString() + " 成功");

        }
    }
}
