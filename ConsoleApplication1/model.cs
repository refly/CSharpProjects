﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace COM.CK
{
    public class model
    {
        public struct uart_dis_data
        {
            public byte order;         //货单号
            public byte dis_type;     //显示类型
            public byte state;       //1 2  3  4   5
            public string port;       //端口
            public int ele_id;        //电子标签id
            public byte little_ele_id;  //货物地址
            public int send_count;     //发送数量
            public int receive_count;  //收到的数量
        }


        public struct config_led_bell
        {
            public byte order;          //单号
            public int ele_id;        //电子标签id
            public byte num;          //配置编号

            public byte red_on;         //红灯点亮的时间
            public byte red_off;     //红灯关闭的时间
            public byte red_cycle;       //红灯执行的周期数  0  红灯操作无效  255 红灯周期无穷大
            public byte green_on;        //绿灯点亮的时间
            public byte green_off;    //绿灯关闭的时间
            public byte green_cycle;    //绿灯执行的周期数  0  绿灯操作无效  255 绿灯周期无穷大
            public byte blue_on;         //兰灯点亮的时间
            public byte blue_off;     //兰灯关闭的时间
            public byte blue_cycle;       //兰灯执行的周期数  0  兰灯操作无效  255 兰灯周期无穷大

            public byte speak_on;        //蜂鸣器开的时间
            public byte speak_off;      //蜂鸣器关闭的时间
            public byte speak_cycle;      //蜂鸣器执行的周期数  0  蜂鸣器操作无效  255 蜂鸣器周期无穷大
            public byte state;  //装态保留 设为1
        }
        public struct modify_id
        {
            public byte order;        //货单号
            public int new_ele_id;        //修改后的电子标签id
            public int old_ele_id;     //修改前的电子标签id
            public byte state;         //装态保留 设为1
        }
        public struct config_channel_led
        {
 //           public byte order;        //货单号
            public int channel_id;        //修改后的电子标签id
            public byte config_word;     //修改前的电子标签id
            public byte state;         //装态保留 设为1
        }
        public struct close_channel_led
        {
            //           public byte order;        //货单号
            public int channel_id;        //修改后的电子标签id
            //           public int old_ele_id;     //修改前的电子标签id
            public byte state;         //装态保留 设为1
        }
        public struct open_channel_led
        {
            //           public byte order;        //货单号
            public int channel_id;        //修改后的电子标签id
            //           public int old_ele_id;     //修改前的电子标签id
            public byte state;         //装态保留 设为1
        }
        public struct dis_id
        {
            public byte order;        //修改后的电子标签id
            public int ele_id;        //电子标签id
            public byte state; 
        }
        public struct clear_comment
        {
            public byte order;        //货单号
            public int ele_id;        //电子标签id
            public byte state;     //装态保留 设为1
        }
        //public struct channel_lab
        //{
        //    public byte order;         //货单号
        //    public byte dis_type;     //显示类型
        //    public byte state;       //1 2  3  4   5
        //    public string port;       //端口
        //    public int ele_id;        //电子标签id
        //    public byte little_ele_id;  //货物地址
        //    public int send_count;     //发送数量
        //    public int receive_count;  //收到的数量
        //}
        public struct key_config
        {
            public byte order;        //货单号
            public int ele_id;        //电子标签id            
            public byte fn_key;          //翻页键   1 开启   0  关闭 
            public byte confirm_key;     //完成健   1 开启   0  关闭
            public byte modify_key;      //修改建   1 开启   0  关闭
            public byte state;            //装态保留 设为1
 
        }
        public struct clear_order
        {
            public byte order;        //货单号
            //        public int ele_id;        //电子标签id
            public byte state;     //装态保留 设为1
        }
        //public struct tem_hum_comment
        //{
        //    public byte order;         //货单号
        //    public byte dis_type;     //显示类型
        //    public byte state;       //1 2  3  4   5
        //    public string port;       //端口
        //    public int ele_id;        //电子标签id
        //    public byte little_ele_id;  //货物地址
        //    public int send_count;     //发送数量
        //    public int receive_count;  //收到的数量
        //}
    }
}
