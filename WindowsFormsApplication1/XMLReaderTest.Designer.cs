﻿namespace WindowsFormsApplication1
{
    partial class XMLReaderTest
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btn_read = new System.Windows.Forms.Button();
            this.txt_reader = new System.Windows.Forms.TextBox();
            this.txt_show = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // btn_read
            // 
            this.btn_read.Location = new System.Drawing.Point(269, 137);
            this.btn_read.Name = "btn_read";
            this.btn_read.Size = new System.Drawing.Size(75, 23);
            this.btn_read.TabIndex = 0;
            this.btn_read.Text = "读取";
            this.btn_read.UseVisualStyleBackColor = true;
            this.btn_read.Click += new System.EventHandler(this.btn_read_Click);
            // 
            // txt_reader
            // 
            this.txt_reader.Location = new System.Drawing.Point(73, 139);
            this.txt_reader.Name = "txt_reader";
            this.txt_reader.Size = new System.Drawing.Size(151, 21);
            this.txt_reader.TabIndex = 1;
            // 
            // txt_show
            // 
            this.txt_show.Location = new System.Drawing.Point(395, 139);
            this.txt_show.Name = "txt_show";
            this.txt_show.Size = new System.Drawing.Size(176, 21);
            this.txt_show.TabIndex = 2;
            // 
            // XMLReaderTest
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(833, 493);
            this.Controls.Add(this.txt_show);
            this.Controls.Add(this.txt_reader);
            this.Controls.Add(this.btn_read);
            this.Name = "XMLReaderTest";
            this.Text = "XMLReaderTest";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btn_read;
        private System.Windows.Forms.TextBox txt_reader;
        private System.Windows.Forms.TextBox txt_show;
    }
}