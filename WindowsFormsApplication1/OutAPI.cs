﻿using Com.Geek.Beetle.Config;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Com.GeekPlusBeetle
{
    /**********************************************************
   *描述：    
       对外接口
   ***********************************************************/
    public partial class BeetleMain
    {
        /// <summary>
        ///  描述：
        ///     将焦点聚焦到浏览器上
        /// </summary>
        public bool focusOnWebKit()
        {
            this._chromeWebBrowser.Focus();
            Point point = this._chromeWebBrowser.PointToScreen(this._chromeWebBrowser.Location);
            this.SimulateMouseClick(point.X, point.Y);
            return true;
        }

        /// <summary>
        /// 描述：
        ///     获取墙状态信息，信息之间采用英文逗号隔开
        ///     状态码：0--关闭
        ///             1--亮
        ///             2--闪烁
        /// </summary>
        /// <returns>
        ///     返回播种墙状态信息
        /// </returns>
        public String getSeedingWallIndicatorLamps()
        {
            //TODO获取播种墙显示状态
            return _seedingWall.GetLampsStaus();
        }

        /// <summary>
        /// 描述：
        ///     控制指示灯状态。
        /// 参数：指示灯状态列表。逗号隔开
        /// 返回值：0---失败，
        ///         1---成功
        /// </summary>
        /// <returns></returns>
        public Int16 setSeedingWallIndicatorLamps(String status)
        {
            String[] lambStatus = status.Split(',');
            foreach (var strCmd in lambStatus)
            {
                String cellID = strCmd.Substring(0, 2);
                int cmd = Convert.ToInt32(strCmd.Substring(2, 1));
                //this._seedingWall.CellDict[cellID].CellLamp.SwitchLampStatus(cmd);
            }
            return 1;
        }
        public void initializeSeedingWall()
        {
            this._seedingWall.Initialize();
        }
        /// <summary>
        /// 描述：
        ///     获取工作站ID
        /// </summary>
        /// <returns></returns>
        public String getCurrentStationID()
        {
            return CustomConfig.GetStationID();
        }
    }



}
