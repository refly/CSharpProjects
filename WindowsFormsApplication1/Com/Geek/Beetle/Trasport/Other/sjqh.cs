﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO.Ports;
namespace Com.Geek.Beetle.Trasport.Other
{
    public class sjqh
    {

        //使用之前首先实例化：
        public delegate void receive_dis_data(model.uart_dis_data[] dis_data);
        public event receive_dis_data on_dis_event;
        public delegate void send_dis_data_result(byte order, byte result);
        public event send_dis_data_result on_send_dis_result_event;

        public delegate void config_channel(model.config_channel_led data);
        public event config_channel on_config_channel;

        public delegate void close_channel(model.close_channel_led data);
        public event close_channel on_close_channel;

        public delegate void open_channel(model.open_channel_led data);
        public event open_channel on_open_channel;

        static byte[] crc8_table = new byte[256] {0x00, 0x5e, 0xbc, 0xe2, 0x61, 0x3f, 0xdd, 0x83,
                                     0xc2, 0x9c, 0x7e, 0x20, 0xa3, 0xfd, 0x1f, 0x41,
                                     0x9d, 0xc3, 0x21, 0x7f, 0xfc, 0xa2, 0x40, 0x1e,
                                     0x5f, 0x01, 0xe3, 0xbd, 0x3e, 0x60, 0x82, 0xdc,
                                     0x23, 0x7d, 0x9f, 0xc1, 0x42, 0x1c, 0xfe, 0xa0,
                                     0xe1, 0xbf, 0x5d, 0x03, 0x80, 0xde, 0x3c, 0x62,
                                     0xbe, 0xe0, 0x02, 0x5c, 0xdf, 0x81, 0x63, 0x3d,
                                     0x7c, 0x22, 0xc0, 0x9e, 0x1d, 0x43, 0xa1, 0xff,
                                     0x46, 0x18, 0xfa, 0xa4, 0x27, 0x79, 0x9b, 0xc5,
                                     0x84, 0xda, 0x38, 0x66, 0xe5, 0xbb, 0x59, 0x07,
                                     0xdb, 0x85, 0x67, 0x39, 0xba, 0xe4, 0x06, 0x58,
                                     0x19, 0x47, 0xa5, 0xfb, 0x78, 0x26, 0xc4, 0x9a,
                                     0x65, 0x3b, 0xd9, 0x87, 0x04, 0x5a, 0xb8, 0xe6,
                                     0xa7, 0xf9, 0x1b, 0x45, 0xc6, 0x98, 0x7a, 0x24,
                                     0xf8, 0xa6, 0x44, 0x1a, 0x99, 0xc7, 0x25, 0x7b,
                                     0x3a, 0x64, 0x86, 0xd8, 0x5b, 0x05, 0xe7, 0xb9,
                                     0x8c, 0xd2, 0x30, 0x6e, 0xed, 0xb3, 0x51, 0x0f,
                                     0x4e, 0x10, 0xf2, 0xac, 0x2f, 0x71, 0x93, 0xcd,
                                     0x11, 0x4f, 0xad, 0xf3, 0x70, 0x2e, 0xcc, 0x92,
                                     0xd3, 0x8d, 0x6f, 0x31, 0xb2, 0xec, 0x0e, 0x50,
                                     0xaf, 0xf1, 0x13, 0x4d, 0xce, 0x90, 0x72, 0x2c,
                                     0x6d, 0x33, 0xd1, 0x8f, 0x0c, 0x52, 0xb0, 0xee,
                                     0x32, 0x6c, 0x8e, 0xd0, 0x53, 0x0d, 0xef, 0xb1,
                                     0xf0, 0xae, 0x4c, 0x12, 0x91, 0xcf, 0x2d, 0x73,
                                     0xca, 0x94, 0x76, 0x28, 0xab, 0xf5, 0x17, 0x49,
                                     0x08, 0x56, 0xb4, 0xea, 0x69, 0x37, 0xd5, 0x8b,
                                     0x57, 0x09, 0xeb, 0xb5, 0x36, 0x68, 0x8a, 0xd4,
                                     0x95, 0xcb, 0x29, 0x77, 0xf4, 0xaa, 0x48, 0x16,
                                     0xe9, 0xb7, 0x55, 0x0b, 0x88, 0xd6, 0x34, 0x6a,
                                     0x2b, 0x75, 0x97, 0xc9, 0x4a, 0x14, 0xf6, 0xa8,
                                     0x74, 0x2a, 0xc8, 0x96, 0x15, 0x4b, 0xa9, 0xf7,
                                     0xb6, 0xe8, 0x0a, 0x54, 0xd7, 0x89, 0x6b, 0x35};

        private SerialPort commPort = new SerialPort();
        static int order_buff_max = 1200;// 单个端口的订单最大量
        static int order_quantity = 255;  // 订单号
        public bool check_state;//盘点为false  出库为true 默认出库设置
        int address;          //

        model.uart_dis_data[,] send_comment = new model.uart_dis_data[order_quantity, order_buff_max];//当前端口处理的订单内容
        int[] send_comment_size = new int[order_quantity];//不同订单保存订单条数
        int[] receive_comment_size = new int[order_quantity];//
        byte[] write_comment_lab = new byte[order_quantity];//
        public static byte[] current_order_exe_state;// = new byte[serial_count];//新数据标志
        public static bool[] read_new_data_lab;// = new bool[serial_count];//新数据标志
        bool command_test_ack = false;//by dhwu 140808
        bool command_baud_ack = false;//by dhwu 140808
        bool command_searchlight_ack = false;
        bool command_changelightid_ack = false;
        bool command_getinf_ack = false;
        bool command_clear_chled_ack = false;
        public bool command_scan = false;
        public bool command_test_kzq = false;
        public byte[] command_test_buf = new byte[order_buff_max];
        public byte command_bz_ack = 0;//by dhwu 150122
        public byte command_bz_ack_order = 0;//by dhwu 150528
        public int CrcError = 0;
        int ScanError;
        public struct ScanComPre
        {
            public UInt16[] ScanID;
            public byte[] ScanSequence;
            public byte ScanCnt;
        }
        ScanComPre ScanComPreData;

        public bool clear_order(model.clear_order id_data)
        {//7e 02 0002 24 1d 0a 05 05 05 06 06 03 04 04 00 7f
            byte[] aa = new byte[6];
            int count = 0;
            aa[count++] = id_data.order;
            aa[count++] = Convert.ToByte(0xea);
            aa[count++] = Convert.ToByte(0xff);
            aa[count++] = 0x2d;//命令字
            aa[count++] = 0x0;
            return (manage_usart_package(aa));
            //  return true;
        }
        public bool init_wms_sys(int com_count, string t)
        {
            if (commPort.IsOpen)
                return true;
            address = com_count;
            current_order_exe_state = new byte[com_count];
            read_new_data_lab = new bool[com_count];
            commPort.PortName = t; //by dhwu 140805    "COM" + com_count.ToString();
                                   //commPort.PortName = "COM3";
                                   //commPort.PortName = "COM5";
                                   //commPort.PortName = "COM2";
                                   //commPort.PortName = "COM" + com_count.ToString();
            commPort.BaudRate = 9600;
            commPort.Parity = Parity.None;// Parity.Even;
            commPort.DataBits = 8;
            commPort.StopBits = StopBits.One;
            commPort.Encoding = Encoding.ASCII;
            //by dhwu 150420 commPort.ReadBufferSize = 0x5000;//by dhwu 150130
            commPort.DataReceived += new SerialDataReceivedEventHandler(Sp_DataReceived);
            commPort.ReceivedBytesThreshold = 1;
            try
            {
                commPort.Open();
            }
            catch
            {
                return false;
            }
            for (int i = 0; i < order_quantity; i++)
            {
                for (int j = 0; j < order_buff_max; j++)
                {
                    send_comment[i, j].dis_type = 0;
                    send_comment[i, j].ele_id = 0;
                    send_comment[i, j].little_ele_id = 0;
                    send_comment[i, j].order = 0;
                    send_comment[i, j].receive_count = 0;
                    send_comment[i, j].send_count = 0;
                }
                send_comment_size[i] = 0;
                receive_comment_size[i] = 0;
            }
            check_state = true;
            ScanComPreData.ScanCnt = 0;
            ScanComPreData.ScanID = new UInt16[100];
            ScanComPreData.ScanSequence = new byte[100];
            for (int i = 0; i < 100; i++)
            {
                ScanComPreData.ScanID[i] = 0;
                ScanComPreData.ScanSequence[i] = 0xa5;
            }
            return true;
        }
        public void exit_wms_sys()
        {
            close_Port();
        }
        private void manage_data_comment(byte[] read_dat)
        {
            //读取串口数据
            //byte[] read_dat;// = new uart_dis_data[]; 
            //read_dat = uart.manage_data();
            if (read_dat.Length == 0)
                return;
            byte command;
            UInt16 id = Convert.ToUInt16(read_dat[2] * 256 + read_dat[3]);
            if (read_dat.Length != 0)
            {//串口有数据
                command = Convert.ToByte(read_dat[4] & 0x1f);
                //Console.WriteLine("{0}:{1}", command, read_dat.Length);
                if (command == 0x01)
                {//单条显示数据
                    model.uart_dis_data cc;
                    cc = analytic_dis_data_single(read_dat);
                    model.uart_dis_data[] aa = new model.uart_dis_data[1];
                    aa[0] = cc;
                    on_dis_event(aa);

                    byte[] buf = new byte[8];
                    buf[0] = cc.order;// id_data.order;
                    buf[1] = 0;// Convert.ToByte(id_data.ele_id / 256);
                    buf[2] = 0;// Convert.ToByte(id_data.ele_id % 256);
                    buf[3] = 28;//命令字
                    buf[4] = read_dat[2];
                    buf[5] = read_dat[3];
                    buf[6] = cc.little_ele_id;

                    if (manage_usart_package(buf))

                        return;
                }
                else if (command == 0x02)
                {//
                    //
                }
                else if (command == 0x03)
                {//

                }
                else if (command == 0x04)
                {//配置电子标签指示灯和蜂鸣器工作方式

                }
                else if (command == 0x05)
                {//修改电子标签的ID号

                }
                else if (command == 0x06)
                {//显示电子标签的ID号

                }
                else if (command == 0x07)
                {//清除电子标签显示内容

                }
                else if (command == 0x08)
                {//通道灯命令

                }
                else if (command == 0x09)
                {//按键配置命令

                }
                else if (command == 0x0a)
                {//温湿度指令

                }
                else if ((command == 0x0b) || (command == 0x0c))
                {//查询命令
                    model.uart_dis_data[] dd;
                    if ((read_dat[4] >> 5) == 0)
                    {//返回的执行结果
                     //   current_order_exe_state[address] = read_dat[5];
                        if (read_dat[5] == 1)
                        {//执行成功
                            //on_send_dis_result_event(read_dat[1], read_dat[5]);
                            command_bz_ack_order = read_dat[1];
                            command_bz_ack = 1;
                        }
                        else if (read_dat[5] == 5)
                        {//执行失败
                            //on_send_dis_result_event(read_dat[1], read_dat[5]);
                            command_bz_ack_order = read_dat[1];
                            command_bz_ack = 5;
                        }
                        else if (read_dat[5] == 2)
                        {//部分成功
                            ;// on_send_dis_result_event(read_dat[1], read_dat[5]);
                        }
                        else if (read_dat[5] == 3)
                        {// 控制器没有空间  稍后再发送数据
                            on_send_dis_result_event(read_dat[1], read_dat[5]);
                        }
                        else if (read_dat[5] == 4)
                        {//数据部分发送成功
                            //on_send_dis_result_event(read_dat[1], read_dat[5]);
                            command_bz_ack_order = read_dat[1];
                            command_bz_ack = 4;
                        }
                        else
                        {//未知
                            on_send_dis_result_event(read_dat[1], read_dat[5]);
                        }
                    }
                    if ((read_dat[4] >> 5) == 2)
                    {//返回的数据
                        dd = analytic_dis_data_mul(read_dat);
                        byte ord = dd[0].order;
                        for (int i = 0; i < dd.Length; i++)
                        {
                            for (int j = 0; j < send_comment_size[ord]; j++)
                            {
                                if ((dd[i].ele_id == send_comment[ord, j].ele_id) &&
                                    (dd[i].little_ele_id == send_comment[ord, j].little_ele_id))
                                {
                                    if (dd[i].state > send_comment[ord, j].state)
                                    {
                                        send_comment[ord, j].state = dd[i].state;
                                        if (dd[i].state == 5)
                                        {
                                            send_comment[ord, j].receive_count = dd[i].receive_count;
                                            receive_comment_size[ord]++;
                                        }

                                        //read_new_data_lab[address] = true;
                                    }
                                }
                            }
                        }
                        command_bz_ack_order = dd[0].order;
                        command_bz_ack = 6;//by dhwu 150122    on_dis_event(dd);//
                    }
                }
                else if (command == 0x0c)
                {//

                }
                else if (command == 0x0d)
                {//

                }
                else if (command == 0x0e)
                {//

                }
                else if (command == 0x0f)
                {//配置通道灯显示类型
                    model.config_channel_led dd;
                    dd.channel_id = id;
                    dd.config_word = read_dat[5];
                    dd.state = 2;
                    on_config_channel(dd);
                }
                else if (command == 0x10)
                {//关闭通道灯的应答
                    //on_dis_event(dd);
                    model.close_channel_led dd;
                    dd.channel_id = id;
                    dd.state = 2;
                    on_close_channel(dd);

                }
                else if (command == 0x11)
                {//打开通道灯的应答
                    model.open_channel_led dd;
                    dd.channel_id = id;
                    dd.state = 2;
                    on_open_channel(dd);
                }
                else if (command == 0x12)
                {//

                }
                else if (command == 0x15)
                {//测试数据
                    for (int i = 0; i < 15; i++)
                    {
                        command_test_buf[i] = read_dat[i];
                    }
                    command_test_ack = true;
                }
                else if (command == 0x16)
                {//波特率
                    for (int i = 0; i < 6; i++)
                    {
                        command_test_buf[i] = read_dat[i];
                    }
                    command_baud_ack = true;
                }
                else if (command == 0x17)
                {//搜索通道灯
                    for (int i = 0; i < 9; i++)
                    {
                        command_test_buf[i] = read_dat[i];
                    }
                    command_searchlight_ack = true;
                }
                else if (command == 0x18)
                {//修改通道灯id
                    for (int i = 0; i < 9; i++)
                    {
                        command_test_buf[i] = read_dat[i];
                    }
                    command_changelightid_ack = true;
                }
                else if (command == 0x19)
                {//获得设备信息
                    for (int i = 0; i < read_dat.Length; i++)
                    {
                        command_test_buf[i] = read_dat[i];
                    }
                    command_getinf_ack = true;
                }
                else if (command == 0x1a)
                {//扫描枪发来的数据，应答
                    bool FindID;

                    FindID = true;
                    for (int i = 0; i < read_dat.Length; i++)
                    {
                        command_test_buf[i] = read_dat[i];
                    }
                    for (int i = 0; i < ScanComPreData.ScanCnt; i++)
                    {
                        if (id == ScanComPreData.ScanID[i])
                        {
                            if (ScanComPreData.ScanSequence[i] != read_dat[5])
                            {
                                command_scan = true;
                                ScanComPreData.ScanSequence[i] = read_dat[5];
                            }
                            else
                            {
                                ScanError++;
                                //command_scan = true;
                            }
                            FindID = false;
                        }
                    }
                    if (FindID)
                    {
                        command_scan = true;
                        ScanComPreData.ScanID[ScanComPreData.ScanCnt] = id;
                        ScanComPreData.ScanSequence[ScanComPreData.ScanCnt] = read_dat[5];
                        ScanComPreData.ScanCnt++;
                        if (ScanComPreData.ScanCnt > 100)//从新加入
                        {
                            ScanComPreData.ScanCnt = 0;
                            for (int i = 0; i < 100; i++)
                            {
                                ScanComPreData.ScanID[i] = 0;
                                ScanComPreData.ScanSequence[i] = 0xa5;
                            }
                        }
                    }
                    byte[] aa = new byte[5];
                    aa[0] = read_dat[5];
                    aa[1] = Convert.ToByte(read_dat[2]);
                    aa[2] = Convert.ToByte(read_dat[3]);
                    aa[3] = 0x1a;//命令字
                    manage_usart_package(aa);
                }
                else if (command == 0x1b)
                {
                    for (int i = 0; i < read_dat.Length; i++)
                    {
                        command_test_buf[i] = read_dat[i];
                    }
                    command_test_kzq = true;
                }
                else if (command == 0x1d)
                {
                    for (int i = 0; i < read_dat.Length; i++)
                    {
                        command_test_buf[i] = read_dat[i];
                    }
                    command_clear_chled_ack = true;
                }
            }
        }
        int count = 0;
        public void Sp_DataReceived(object sender, System.IO.Ports.SerialDataReceivedEventArgs e)
        {
            //System.Threading.Thread.Sleep(20);//by dhwu 150123 20ms
            byte[] readBuffer = new byte[commPort.BytesToRead];
            commPort.Read(readBuffer, 0, readBuffer.Length);

            if (readBuffer.Length > 25)
            {
                count++;
            }


            byte[] aa = manage_data(readBuffer);
            manage_data_comment(aa);
            //Console.WriteLine("C{0}:{1}",count, readBuffer.Length);
            count++;
            while (true)
            {
                byte[] ss = manage_data_package();
                if (ss.Length != 0)
                {
                    manage_data_comment(ss);
                }
                else
                {
                    return;
                }
            }
        }
        public bool write_dis_comment(model.uart_dis_data[] dis_data)
        {
            write_command(dis_data);
            return true;
        }

        private byte[] single_config(byte cycle, byte on, byte off)
        {
            if (cycle == 0)
            {
                byte[] result = new byte[0];
                return result;
            }
            else
            {
                byte[] result = new byte[3];
                result[0] = cycle;
                result[1] = on;
                result[2] = off;
                return result;
            }
        }

        public bool write_config_comment(model.config_led_bell config_data)
        {//7e 02 0002 24 1d 0a 05 05 05 06 06 03 04 04 00 7f
         //write_command(dis_data);
         //计算长度a
         //             3 	2	1	0
         //             红	绿	蓝	蜂鸣器
            byte config_id = config_data.num;
            // byte 
            byte[] result = single_config(config_data.red_cycle, config_data.red_on, config_data.red_off);
            if (result.Length == 0)
            {
                config_id = Convert.ToByte(config_id << 1);
            }
            else
            {
                config_id = Convert.ToByte((config_id << 1) + 1);
            }
            byte[] result1 = single_config(config_data.green_cycle, config_data.green_on, config_data.green_off);
            if (result1.Length == 0)
            {
                config_id = Convert.ToByte(config_id << 1);
            }
            else
            {
                config_id = Convert.ToByte((config_id << 1) + 1);
            }

            byte[] result2 = single_config(config_data.blue_cycle, config_data.blue_on, config_data.blue_off);
            if (result2.Length == 0)
            {
                config_id = Convert.ToByte(config_id << 1);
            }
            else
            {
                config_id = Convert.ToByte((config_id << 1) + 1);
            }
            byte[] result3 = single_config(config_data.speak_cycle, config_data.speak_on, config_data.speak_off);
            if (result3.Length == 0)
            {
                config_id = Convert.ToByte(config_id << 1);
            }
            else
            {
                config_id = Convert.ToByte((config_id << 1) + 1);
            }

            int count = result.Length + result1.Length + result2.Length + result3.Length + 6;
            byte[] aa = new byte[count];
            count = 0;
            aa[count++] = config_data.order;
            aa[count++] = Convert.ToByte(config_data.ele_id / 256);
            aa[count++] = Convert.ToByte(config_data.ele_id % 256); ;
            aa[count++] = 0x24;//命令字
            aa[count++] = config_id;
            for (int i = 0; i < result.Length; i++)
            {
                aa[count++] = result[i];
            }
            for (int i = 0; i < result1.Length; i++)
            {
                aa[count++] = result1[i];
            }
            for (int i = 0; i < result2.Length; i++)
            {
                aa[count++] = result2[i];
            }
            for (int i = 0; i < result3.Length; i++)
            {
                aa[count++] = result3[i];
            }
            return (manage_usart_package(aa));
            //  return true;
        }


        public bool write_modify_id_comment(model.modify_id id_data)
        {//7e 02 0002 24 1d 0a 05 05 05 06 06 03 04 04 00 7f
            byte[] aa = new byte[7];
            int count = 0;
            aa[count++] = id_data.order;
            aa[count++] = Convert.ToByte(id_data.old_ele_id / 256);
            aa[count++] = Convert.ToByte(id_data.old_ele_id % 256);
            aa[count++] = 0x25;//命令字
            aa[count++] = Convert.ToByte(id_data.new_ele_id / 256);
            aa[count++] = Convert.ToByte(id_data.new_ele_id % 256);
            return (manage_usart_package(aa));
            //  return true;
        }
        public bool write_config_channel_led(model.config_channel_led id_data)
        {//7e 02 0002 24 1d 0a 05 05 05 06 06 03 04 04 00 7f
            byte[] aa = new byte[6];
            int count = 0;
            aa[count++] = 0;
            aa[count++] = Convert.ToByte(id_data.channel_id / 256);
            aa[count++] = Convert.ToByte(id_data.channel_id % 256);
            aa[count++] = 0x2f;//命令字
            aa[count++] = id_data.config_word;
            //aa[count++] = Convert.ToByte(id_data.new_ele_id / 256);
            //aa[count++] = Convert.ToByte(id_data.new_ele_id % 256);
            return (manage_usart_package(aa));
            //  return true;
        }

        public bool write_close_channel_led(model.close_channel_led id_data)
        {//7e 02 0002 24 1d 0a 05 05 05 06 06 03 04 04 00 7f
            byte[] aa = new byte[5];
            int count = 0;
            aa[count++] = 0;
            aa[count++] = Convert.ToByte(id_data.channel_id / 256);
            aa[count++] = Convert.ToByte(id_data.channel_id % 256);
            aa[count++] = 0x30;//命令字
            return (manage_usart_package(aa));
            //  return true;
        }
        public bool clear_channel_led(model.close_channel_led id_data)
        {//7e 02 0002 24 1d 0a 05 05 05 06 06 03 04 04 00 7f
            byte[] aa = new byte[5];
            int count = 0;
            aa[count++] = 0;
            aa[count++] = Convert.ToByte(id_data.channel_id / 256);
            aa[count++] = Convert.ToByte(id_data.channel_id % 256);
            aa[count++] = 0x3d;//命令字
            if (manage_usart_package(aa))
            {
                int Timeout = 100;
                do
                {
                    System.Threading.Thread.Sleep(3);
                    if (command_clear_chled_ack)
                    {
                        command_clear_chled_ack = false;
                        return true;
                    }
                    Timeout--;
                }
                while (Timeout > 0);
            }
            return false;
        }
        public bool write_open_channel_led(model.open_channel_led id_data)
        {//7e 02 0002 24 1d 0a 05 05 05 06 06 03 04 04 00 7f
            byte[] aa = new byte[5];
            int count = 0;
            aa[count++] = 0;
            aa[count++] = Convert.ToByte(id_data.channel_id / 256);
            aa[count++] = Convert.ToByte(id_data.channel_id % 256);
            aa[count++] = 0x31;//命令字
            return (manage_usart_package(aa));
            //  return true;
        }

        //public static bool write_dis_id_comment(dis_id dis_data)
        public bool write_dis_id_comment(model.dis_id id_data)
        {//7e 02 0002 24 1d 0a 05 05 05 06 06 03 04 04 00 7f
            byte[] aa = new byte[5];
            int count = 0;
            aa[count++] = id_data.order;
            aa[count++] = Convert.ToByte(id_data.ele_id / 256);
            aa[count++] = Convert.ToByte(id_data.ele_id % 256);
            aa[count++] = 0x26;//命令字
                               //aa[count++] = Convert.ToByte(id_data.new_ele_id / 256);
                               //aa[count++] = Convert.ToByte(id_data.new_ele_id % 256);
            return (manage_usart_package(aa));
            //  return true;
        }
        //by dhwu 140807
        //复位各个设备  
        public bool write_reset_device(model.dis_id id_data)
        {
            //7E 00 FF FF 34 67 7F 7E 01 00 00 34 BB 7F 
            //7e 02 0002 24 1d 0a 05 05 05 06 06 03 04 04 00 7f
            byte[] aa = new byte[5];
            int count = 0;
            aa[count++] = id_data.order;
            aa[count++] = Convert.ToByte(id_data.ele_id / 256);
            aa[count++] = Convert.ToByte(id_data.ele_id % 256);
            aa[count++] = 0x34;//命令字
                               //aa[count++] = Convert.ToByte(id_data.new_ele_id / 256);
                               //aa[count++] = Convert.ToByte(id_data.new_ele_id % 256);
            return (manage_usart_package(aa));
            //  return true;
        }
        public bool write_baud_device(model.dis_id id_data, byte dat)
        {//7e 02 0002 24 1d 0a 05 05 05 06 06 03 04 04 00 7f
            byte[] aa = new byte[6];
            int count = 0;
            aa[count++] = id_data.order;
            aa[count++] = Convert.ToByte(id_data.ele_id / 256);
            aa[count++] = Convert.ToByte(id_data.ele_id % 256);
            aa[count++] = 0x36;//命令字
            aa[count++] = dat;
            if ((manage_usart_package(aa)) && (id_data.order == 0))
            {
                int Timeout = 100;
                do
                {
                    System.Threading.Thread.Sleep(3);
                    if (command_baud_ack)
                    {
                        command_baud_ack = false;
                        if (aa[4] != command_test_buf[5]) return false;
                        return true;
                    }
                    Timeout--;
                }
                while (Timeout > 0);
            }
            return false;
        }
        public bool write_test_device(model.dis_id id_data, byte n)
        {//7e 02 0002 24 1d 0a 05 05 05 06 06 03 04 04 00 7f
            byte[] aa = new byte[15];
            byte i;

            aa[0] = id_data.order;
            aa[1] = Convert.ToByte(id_data.ele_id / 256);
            aa[2] = Convert.ToByte(id_data.ele_id % 256);
            aa[3] = 0x35;//命令字
            for (i = 0; i < 10; i++)
            {
                aa[4 + i] = (byte)(i + n);
            }
            if ((manage_usart_package(aa)) && (id_data.order == 0))
            {
                int Timeout = 100;
                do
                {
                    System.Threading.Thread.Sleep(3);
                    if (command_test_ack)
                    {
                        command_test_ack = false;
                        for (i = 0; i < 10; i++)
                        {
                            if (aa[4 + i] != command_test_buf[5 + i])
                                return false;
                        }
                        return true;
                    }
                    Timeout--;
                }
                while (Timeout > 0);
            }
            return false;
        }
        public bool change_light_id(model.modify_id id_data)
        {
            byte[] aa = new byte[7];
            byte i;

            aa[0] = id_data.order;
            aa[1] = Convert.ToByte(id_data.old_ele_id / 256);
            aa[2] = Convert.ToByte(id_data.old_ele_id % 256);
            aa[3] = 0x38;//命令字
            aa[4] = Convert.ToByte(id_data.new_ele_id / 256);
            aa[5] = Convert.ToByte(id_data.new_ele_id % 256);

            if ((manage_usart_package(aa)) && (id_data.order == 0))
            {
                int Timeout = 100;
                do
                {
                    System.Threading.Thread.Sleep(3);
                    if (command_changelightid_ack)
                    {
                        command_changelightid_ack = false;
                        aa[3] = 0x58;
                        for (i = 0; i < 5; i++)
                        {
                            if (aa[1 + i] != command_test_buf[2 + i])
                                return false;
                        }
                        return true;
                    }
                    Timeout--;
                }
                while (Timeout > 0);
            }
            return false;
        }
        public bool search_light_id(model.modify_id id_data)
        {
            byte[] aa = new byte[7];
            byte i;

            aa[0] = id_data.order;
            aa[1] = Convert.ToByte(id_data.old_ele_id / 256);
            aa[2] = Convert.ToByte(id_data.old_ele_id % 256);
            aa[3] = 0x37;//命令字
            aa[4] = Convert.ToByte(id_data.new_ele_id / 256);
            aa[5] = Convert.ToByte(id_data.new_ele_id % 256);

            if ((manage_usart_package(aa)) && (id_data.order == 0))
            {
                int Timeout = 100;
                do
                {
                    System.Threading.Thread.Sleep(3);
                    if (command_searchlight_ack)
                    {
                        command_searchlight_ack = false;
                        for (i = 0; i < 2; i++)
                        {
                            if (aa[1 + i] != command_test_buf[5 + i])
                                return false;
                        }
                        return true;
                    }
                    Timeout--;
                }
                while (Timeout > 0);
            }
            return false;
        }
        //读取设备信息
        public bool get_inf_device(model.dis_id id_data)
        {//7e 02 0002 24 1d 0a 05 05 05 06 06 03 04 04 00 7f
            byte[] aa = new byte[5];
            int count = 0;
            byte i;
            aa[count++] = id_data.order;
            aa[count++] = Convert.ToByte(id_data.ele_id / 256);
            aa[count++] = Convert.ToByte(id_data.ele_id % 256);
            aa[count++] = 0x39;//命令字
            if ((manage_usart_package(aa)) && (id_data.order == 0))
            {
                int Timeout = 100;
                do
                {
                    System.Threading.Thread.Sleep(3);
                    if (command_getinf_ack)
                    {
                        command_getinf_ack = false;
                        aa[3] = 0x59;
                        for (i = 1; i < 5; i++)
                        {
                            if (aa[i - 1] != command_test_buf[i])
                                return false;
                        }
                        return true;
                    }
                    Timeout--;
                }
                while (Timeout > 0);
            }
            return false;
        }
        public bool send_kzq_test(byte s, int len)
        {//7e 02 0002 24 1d 0a 05 05 05 06 06 03 04 04 00 7f
            byte[] aa = new byte[len + 5];
            int count = 0;
            int i;
            aa[count++] = 0;// id_data.order;
            aa[count++] = 0;// Convert.ToByte(id_data.ele_id / 256);
            aa[count++] = 0;// Convert.ToByte(id_data.ele_id % 256);
            aa[count++] = 0x3b;//命令字
            for (i = 0; i < len; i++)
            {
                aa[count++] = (byte)(i + s);
            }
            if (manage_usart_package(aa))
            {
                return true;
            }
            else return false;
        }
        //end dhwu

        public bool write_clear_comment(model.clear_comment id_data)
        {//7e 02 0002 24 1d 0a 05 05 05 06 06 03 04 04 00 7f
            byte[] aa = new byte[5];
            int count = 0;
            aa[count++] = id_data.order;
            aa[count++] = Convert.ToByte(id_data.ele_id / 256);
            aa[count++] = Convert.ToByte(id_data.ele_id % 256);
            aa[count++] = 0x27;//命令字
                               //aa[count++] = Convert.ToByte(id_data.new_ele_id / 256);
                               //aa[count++] = Convert.ToByte(id_data.new_ele_id % 256);
            return (manage_usart_package(aa));
            //  return true;
        }
        public bool write_key_config(model.key_config config)
        {//7e 02 0002 24 1d 0a 05 05 05 06 06 03 04 04 00 7f
            byte config_key = 0;
            if (config.modify_key == 1)
                config_key = Convert.ToByte((config_key << 1) + 1);
            else
                config_key = Convert.ToByte(config_key << 1);

            if (config.confirm_key == 1)
                config_key = Convert.ToByte((config_key << 1) + 1);
            else
                config_key = Convert.ToByte(config_key << 1);

            if (config.fn_key == 1)
                config_key = Convert.ToByte((config_key << 1) + 1);
            else
                config_key = Convert.ToByte(config_key << 1);

            byte[] aa = new byte[6];


            int count = 0;
            aa[count++] = config.order;
            aa[count++] = Convert.ToByte(config.ele_id / 256);
            aa[count++] = Convert.ToByte(config.ele_id % 256);
            aa[count++] = 0x29;//命令字
            aa[count++] = config_key;//命令字
                                     //aa[count++] = Convert.ToByte(id_data.new_ele_id / 256);
                                     //aa[count++] = Convert.ToByte(id_data.new_ele_id % 256);
            return (manage_usart_package(aa));
            //  return true;
        }


        //      private readonly StringBuilder strReceive = new StringBuilder(); // 接收到数据的缓冲区  
        //    public model.uart_dis_data ss;// = new uart_dis_data();
        private int receive_max = 20000;
        private byte[] receive_buff = new byte[20000];
        private byte[] package_buff = new byte[20000];
        private int receive_point;
        private int manage_point;
        private int package_point;
        private bool package_head_lab;
        private bool package_end_lab;
        private bool change_lab;
        private byte package_head = 0x7e;
        private byte package_end = 0x7f;
        private byte change_byte = 0x20;

        //把串口的数据保存起来
        private void read_package(byte[] buf)
        {
            for (int i = 0; i < buf.Length; i++)
            {
                receive_buff[receive_point++] = buf[i];
                if (receive_point >= receive_max)
                    receive_point = 0;
            }
        }
        /**********************************************************************************
        接收数据
        //串口中断接收数据
        //本函数是接收一包完整的数据  接收完后设置package_head_lab  package_end_lab为success
        处理完本包数据后才能接收下一包数据
        ***********************************************************************************/
        private void receive_manage(byte[] data)
        {
            read_package(data);
            while (true)
            {
                if (receive_point == manage_point)
                    return;
                //write_ascii(receive_buff[manage_point]);
                if (package_head_lab == false)
                {//接收包头
                    if (receive_buff[manage_point] == package_head)
                    {
                        //write_ascii(receive_buff[manage_point]);
                        receive_buff[manage_point] = 0;
                        package_point = 0;
                        package_buff[package_point++] = package_head;
                        if (package_point >= receive_max) package_point = receive_max - 1;//by dhwu 150207
                        package_head_lab = true;
                        manage_point++;
                        if (manage_point >= receive_max)
                            manage_point = 0;
                    }
                    else
                    {
                        receive_buff[manage_point] = 0;
                        manage_point++;
                        if (manage_point >= receive_max)
                            manage_point = 0;
                    }
                }
                else
                {  //接收包
                    if (package_end_lab == true)
                        return;

                    if (receive_buff[manage_point] == package_end)
                    {
                        //write_ascii(receive_buff[manage_point]);
                        receive_buff[manage_point] = 0;
                        package_end_lab = true;
                        package_buff[package_point++] = package_end;
                        if (package_point >= receive_max) package_point = receive_max - 1;
                        manage_point++;
                        if (manage_point >= receive_max)
                            manage_point = 0;
                        return;
                    }
                    else
                    {
                        if (receive_buff[manage_point] == change_byte)
                        {
                            // write_ascii(receive_buff[manage_point]);
                            change_lab = true;
                            manage_point++;
                            if (manage_point >= receive_max)
                                manage_point = 0;
                        }
                        else
                        {
                            if (change_lab == true)
                            {
                                change_lab = false;
                                //write_ascii(receive_buff[manage_point]);
                                if (receive_buff[manage_point] == 0)
                                {
                                    package_buff[package_point++] = package_head;
                                }
                                else if (receive_buff[manage_point] == 1)
                                {
                                    package_buff[package_point++] = package_end;
                                }
                                else if (receive_buff[manage_point] == 2)
                                {
                                    package_buff[package_point++] = change_byte;
                                }
                                else
                                {
                                    package_head_lab = false;
                                    package_end_lab = false;
                                    receive_buff[manage_point] = 0;
                                    manage_point++;
                                    if (manage_point >= receive_max)
                                        manage_point = 0;
                                    return;
                                }
                                receive_buff[manage_point] = 0;
                                manage_point++;
                                if (manage_point >= receive_max)
                                    manage_point = 0;

                            }
                            else
                            {
                                //write_ascii(receive_buff[manage_point]);
                                package_buff[package_point++] = receive_buff[manage_point];
                                receive_buff[manage_point] = 0;
                                manage_point++;
                                if (manage_point >= receive_max)
                                    manage_point = 0;
                            }
                            if (package_point >= receive_max) package_point = receive_max - 1;
                        }
                    }
                }
            }
        }
        /**********************************************************************************
            接收数据
            //串口中断接收数据
            //本函数是接收一包完整的数据  接收完后设置package_head_lab  package_end_lab为success
            处理完本包数据后才能接收下一包数据
            ***********************************************************************************/
        private void receive_manage()
        {
            //read_package(data);
            while (true)
            {
                if (receive_point == manage_point)
                    return;
                //write_ascii(receive_buff[manage_point]);
                if (package_head_lab == false)
                {//接收包头
                    if (receive_buff[manage_point] == package_head)
                    {
                        //write_ascii(receive_buff[manage_point]);
                        receive_buff[manage_point] = 0;
                        package_point = 0;
                        package_buff[package_point++] = package_head;
                        if (package_point >= receive_max) package_point = receive_max - 1;
                        package_head_lab = true;
                        manage_point++;
                        if (manage_point >= receive_max)
                            manage_point = 0;
                    }
                    else
                    {
                        receive_buff[manage_point] = 0;
                        manage_point++;
                        if (manage_point >= receive_max)
                            manage_point = 0;
                    }
                }
                else
                {  //接收包
                    if (package_end_lab == true)
                        return;

                    if (receive_buff[manage_point] == package_end)
                    {
                        //write_ascii(receive_buff[manage_point]);
                        receive_buff[manage_point] = 0;
                        package_end_lab = true;
                        package_buff[package_point++] = package_end;
                        if (package_point >= receive_max) package_point = receive_max - 1;
                        manage_point++;
                        if (manage_point >= receive_max)
                            manage_point = 0;
                        return;
                    }
                    else
                    {
                        if (receive_buff[manage_point] == change_byte)
                        {
                            // write_ascii(receive_buff[manage_point]);
                            change_lab = true;
                            manage_point++;
                            if (manage_point >= receive_max)
                                manage_point = 0;
                        }
                        else
                        {
                            if (change_lab == true)
                            {
                                change_lab = false;
                                //write_ascii(receive_buff[manage_point]);
                                if (receive_buff[manage_point] == 0)
                                {
                                    package_buff[package_point++] = package_head;
                                }
                                else if (receive_buff[manage_point] == 1)
                                {
                                    package_buff[package_point++] = package_end;
                                }
                                else if (receive_buff[manage_point] == 2)
                                {
                                    package_buff[package_point++] = change_byte;
                                }
                                else
                                {
                                    package_head_lab = false;
                                    package_end_lab = false;
                                    receive_buff[manage_point] = 0;
                                    manage_point++;
                                    if (manage_point >= receive_max)
                                        manage_point = 0;
                                    return;
                                }
                                receive_buff[manage_point] = 0;
                                manage_point++;
                                if (manage_point >= receive_max)
                                    manage_point = 0;

                            }
                            else
                            {
                                //write_ascii(receive_buff[manage_point]);
                                package_buff[package_point++] = receive_buff[manage_point];
                                receive_buff[manage_point] = 0;
                                manage_point++;
                                if (manage_point >= receive_max)
                                    manage_point = 0;
                            }
                            if (package_point >= receive_max) package_point = receive_max - 1;
                        }
                    }
                }
            }
        }
        /***************************************************************************
数据处理
****************************************************************************/
        private byte[] manage_data(byte[] data)
        {
            //处理串口通过中断接收的数据
            receive_manage(data);

            if ((package_head_lab == true) && (package_end_lab == true))
            { //收到一包数据  处理数据 
                byte[] result = new byte[package_point];
                for (int i = 0; i < package_point; i++)
                    result[i] = package_buff[i];
                package_point = 0;
                package_head_lab = false;
                package_end_lab = false;
                //by dhwu 150123 return result;
                if (crc_base_result(result) == true)
                {
                    return result;
                }
                else
                {
                    byte[] result1 = new byte[0];
                    return result1;
                }
            }
            else
            {
                byte[] result = new byte[0];
                return result;
            }
        }
        private byte[] manage_data_package()
        {
            //处理串口通过中断接收的数据
            receive_manage();

            if ((package_head_lab == true) && (package_end_lab == true))
            { //收到一包数据  处理数据 
              //package_point


                byte[] result = new byte[package_point];
                for (int i = 0; i < package_point; i++)
                    result[i] = package_buff[i];

                package_point = 0;
                package_head_lab = false;
                package_end_lab = false;
                if (crc_base_result(result) == true)
                {
                    return result;
                }
                else
                {
                    byte[] result1 = new byte[0];
                    return result1;
                }
            }
            else
            {
                byte[] result = new byte[0];
                return result;
            }
        }

        //public bool init_Port(string com_num)
        //{
        //    commPort.PortName = com_num;
        //    commPort.BaudRate = 9600;
        //    commPort.Parity = Parity.None;// Parity.Even;
        //    commPort.DataBits = 8;
        //    commPort.StopBits = StopBits.One;
        //    commPort.Encoding = Encoding.ASCII;
        //    try
        //    {
        //        commPort.Open();
        //    }
        //    catch (InvalidOperationException e)
        //    {
        //        return false;
        //    }
        //    // commPort.Write("123123123");
        //    return true;
        //}
        /*
        public bool write_command(uart_channel_comment[]  comment)
            {
                
                return true;
            }

            public bool write_command(uart_clear_comment[] comment)
            {

                return true;
            }
            public bool write_command(uart_clear_orders [] comment)
            {

                return true;
            }
            public bool write_command(uart_config_data[] comment)
            {

                return true;
            }
        */
        public bool write_command(model.uart_dis_data[] comment)
        {
            make_uart_data(comment);
            return true;
        }

        public string close_Port()
        {
            commPort.Close();
            return "ok";
        }
        //
        private bool write_byte(byte byt)
        {
            byte[] byt1 = new byte[1];
            byt1[0] = byt;
            if (commPort.IsOpen)
            {
                try
                {
                    commPort.Write(byt1, 0, 1);
                }
                catch
                {
                    return false;
                }
                while (commPort.BytesToWrite != 0) ;
            }
            else
            {
                try
                {
                    commPort.Open();
                }
                catch
                {
                    return false;
                }
                if (commPort.IsOpen)
                {
                    try
                    {
                        commPort.Write(byt1, 0, 1);
                    }
                    catch
                    {
                        return false;
                    }
                    while (commPort.BytesToWrite != 0) ;
                }
            }
            return true;
        }


        //  public 

        /************************************************
         * 转化成发送数据
         * ***********************************************/

        /**********************
         * 设置检验码
         * ***********************/
        private bool crc_base_resultttt(byte[] comment)
        {

            byte crc8 = 0xff;
            int i;
            for (i = 0; i < comment.Length - 1; i++)//by dhwu 150123
            {
                crc8 = crc8_table[(crc8 ^ comment[i]) & 0xff];

            }
            if (comment[i] == crc8)
            {
                return true;
            }
            else
            {
                CrcError++;
                return false;
            }
            //return comment;
        }
        private byte[] crc_base(byte[] comment)
        {

            byte crc8 = 0xff;
            int i;
            for (i = 0; i < comment.Length - 1; i++)
            {
                crc8 = crc8_table[(crc8 ^ comment[i]) & 0xff];

            }
            comment[i] = crc8;
            if (crc_base_resultttt(comment) != true)
                Console.WriteLine("xxx{0}:{1}", crc8, comment[i]); ;
            return comment;
        }

        private bool crc_base_result(byte[] comment)
        {

            byte crc8 = 0xff;
            int i;
            for (i = 1; i < comment.Length - 2; i++)//by dhwu 150123
            {
                crc8 = crc8_table[(crc8 ^ comment[i]) & 0xff];

            }
            if (comment[i] == crc8)
            {
                return true;
            }
            else
            {
                CrcError++;
                return false;
            }
            //return comment;
        }

        private byte[] set_check_data(byte[] dat)
        {
            return crc_base(dat);
            //int i;
            //int result;
            //result = 0;

            //for (i = 0; i < dat.Length - 1; i++)
            //{
            //    result = result + dat[i];// result + dat[i];
            //    result = result % 256;
            //}
            //dat[i] = Convert.ToByte(result);
            //return dat;
        }

        //向串口发送一包数据
        private bool send_package_dat(byte[] uart_package, int size)
        {
            // int send_size;
            write_byte(0x7e);
            for (int i = 0; i < uart_package.Count(); i++)
            {
                if (uart_package[i] == 0x7e)
                {
                    write_byte(0x20);
                    write_byte(0x00);
                }
                else if (uart_package[i] == 0x7f)
                {
                    write_byte(0x20);
                    write_byte(0x01);
                }
                else if (uart_package[i] == 0x20)
                {
                    write_byte(0x20);
                    write_byte(0x02);
                }
                else
                {
                    write_byte(uart_package[i]);
                }
            }
            write_byte(0x7f);
            return true;
        }



        private bool manage_usart_package(byte[] usart_data)
        {
            byte[] dat;
            dat = set_check_data(usart_data);
            if (send_package_dat(dat, dat.Length) == false)
            {
                return false;
            }
            return true;
        }
        private bool write_usart_package(byte[] usart_data)
        {
            byte[] dat;
            dat = set_check_data(usart_data);
            if (send_package_dat(dat, dat.Length) == false)
            {
                return false;
            }
            return true;
        }

        private bool make_uart_data(model.uart_dis_data[] dat)
        {

            byte[] mak_dis_pack = new byte[dat.Length * 9 + 7];
            //
            int count = 0;
            mak_dis_pack[count++] = dat[0].order;//货单号
            mak_dis_pack[count++] = 0xea;//批处理地址
            mak_dis_pack[count++] = 0xff;
            mak_dis_pack[count++] = 0x2b;
            int aa = dat.Length;
            mak_dis_pack[count++] = Convert.ToByte(aa / 256);//批处理数量
            mak_dis_pack[count++] = Convert.ToByte(aa % 256);
            //int address;
            byte little_id_base_address = new byte();
            // byte little_id_offset_address;
            //byte[] little_id_comment = new byte[2];
            // string litt_comment;
            byte num_base_address = new byte();
            // byte num_offset_address;
            //byte[] num_comment = new byte[4];
            //string num_commen;
            little_id_base_address = 0;//地址基地址
            num_base_address = 8 + 4;      //数量基地址
            if (check_state == true)
            {
                num_base_address = 4;
            }
            else
            {
                num_base_address = 8 + 4;
            }
            for (int i = 0; i < dat.Length; i++)
            {
                //litt_comment = dat[i].little_ele_id.ToString();
                //num_commen = dat[i].send_count.ToString();
                //地址偏移量
                //little_id_offset_address =Convert.ToByte(litt_comment.Count()-1);
                //little_id_comment = get_array(little_id_comment, dat[i].little_ele_id);
                //数量偏移量
                // num_offset_address = Convert.ToByte((dat[i].send_count.ToString()).Count());
                //num_comment = get_array(num_comment, dat[i].send_count);

                mak_dis_pack[count++] = Convert.ToByte(5 + 3);//长度
                mak_dis_pack[count++] = Convert.ToByte(dat[i].ele_id / 256);//id
                mak_dis_pack[count++] = Convert.ToByte(dat[i].ele_id % 256);
                mak_dis_pack[count++] = 0x21;//命令 + 状态

                mak_dis_pack[count] = Convert.ToByte(dat[i].dis_type << 4);//显示类型
                mak_dis_pack[count] = Convert.ToByte(mak_dis_pack[count] | little_id_base_address);//地址的位置
                count++;
                //dat[i].little_ele_id;
                mak_dis_pack[count++] = Convert.ToByte((num_base_address << 4));//地址的位置
                mak_dis_pack[count++] = dat[i].little_ele_id;//地址

                mak_dis_pack[count++] = Convert.ToByte(dat[i].send_count / 256);//数量
                mak_dis_pack[count++] = Convert.ToByte(dat[i].send_count % 256);

            }
            mak_dis_pack[count] = 0;//校验
            manage_usart_package(mak_dis_pack);
            return true;
        }
        //  public int get_ele_id(byte)
        //解析串口数据
        //
        private model.uart_dis_data[] analytic_dis_data_mul(byte[] comment)
        {
            int address, size, offset;
            byte[] buff = new byte[12];
            size = comment[5] * 256 + comment[6];
            model.uart_dis_data[] dis_dat = new model.uart_dis_data[size];
            address = 7;
            for (int i = 0; i < size; i++)
            {
                offset = comment[address];
                dis_dat[i].dis_type = Convert.ToByte(comment[address + 5] >> 4);
                dis_dat[i].state = Convert.ToByte(comment[address + 4] >> 5);
                dis_dat[i].ele_id = comment[address + 2] * 256 + comment[address + 3];
                dis_dat[i].little_ele_id = comment[address + 7];
                dis_dat[i].receive_count = comment[address + 8] * 256 + comment[address + 9];
                dis_dat[i].order = comment[address + 1];

                address = address + offset + 1;
            }
            return dis_dat;
        }
        private model.uart_dis_data analytic_dis_data_single(byte[] comment)
        {
            model.uart_dis_data dis_dat = new model.uart_dis_data();
            int address = 1;
            // offset = comment[address];
            dis_dat.dis_type = Convert.ToByte(comment[address + 4] >> 4);
            dis_dat.ele_id = comment[address + 1] * 256 + comment[address + 2];
            dis_dat.little_ele_id = comment[address + 6];
            dis_dat.receive_count = comment[address + 7] * 256 + comment[address + 8];
            dis_dat.order = comment[1];
            dis_dat.state = Convert.ToByte(comment[4] >> 5);
            // address = address + offset + 1;
            return dis_dat;
        }
        /*private bool get_package_data(model.uart_dis_data[] dat, model.uart_dis_data aa)
        {
            if (dat.Length == 0)
                return false;
            return true;
        }*/
        private bool search_order(byte order)
        {
            //想串口写读取指令
            //7e02eaff2c007f
            byte[] comment = new byte[5];
            int i;
            i = 0;
            //comment[0] = 0x7e;
            comment[i++] = order;
            comment[i++] = 0xea;
            comment[i++] = 0xff;
            comment[i++] = 0x2c;
            comment[i++] = 0x00;
            //comment[i++] = 0x7f;
            //send_package_dat(comment,comment.Count());
            //等待数据返回
            //model.uart_dis_data[] data;

            return true;

        }
    }

}
