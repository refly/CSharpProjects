﻿using Com.Geek.Beetle.Trasport.Other;
using System;
using System.Collections.Generic;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Com.Geek.Beetle.Protocol;

namespace Com.Geek.Beetle.Trasport
{
    /// <summary>
    /// 按钮被点击委托
    /// </summary>
    /// <returns></returns>
    public delegate void ButtonPressedHandler(String id);
    class SerialTrasport
    {
        /// <summary>
        /// 按钮点击事件
        /// </summary>
        public event ButtonPressedHandler ButtonPressedEvent;

        public static sjqh COMPORT_CONTROLLER = new sjqh();
        /// <summary>
        /// 服务器运行状态
        /// </summary>
        public Boolean Status
        {
            get; set;
        }
        public void InitializeComPort(String comPortName)
        {
            bool initResult = COMPORT_CONTROLLER.init_wms_sys(1, comPortName.ToUpper());
            if (!initResult)
                throw new Exception("端口初始化失败");
        }

        /// <summary>
        /// 描述：com口操作层初始换
        /// </summary>
        /// <param name="comPortName">com口的名称</param>
        public void Initialize()
        {
            COMPORT_CONTROLLER.check_state = true;
            COMPORT_CONTROLLER.on_dis_event += new sjqh.receive_dis_data(HandleReceive);
            COMPORT_CONTROLLER.on_send_dis_result_event += new sjqh.send_dis_data_result(receive_dis_data_result);
            COMPORT_CONTROLLER.on_close_channel += new sjqh.close_channel(aa_on_close_channel);
            COMPORT_CONTROLLER.on_config_channel += new sjqh.config_channel(aa_on_config_channel);
            COMPORT_CONTROLLER.on_open_channel += new sjqh.open_channel(aa_on_open_channel);
            //7E 00 FF FF 34 67 7F 
            //7E 01 00 00 34 BB 7F
            model.dis_id id;
            id.order = 0;
            id.ele_id = 65535;
            id.state = 1;
            COMPORT_CONTROLLER.write_reset_device(id);
            System.Threading.Thread.Sleep(100);
            id.order = 1;
            id.ele_id = 0;
            id.state = 1;
            COMPORT_CONTROLLER.write_reset_device(id);
            Thread.Sleep(10000);
        }
        /// <summary>
        /// 发送数据
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public bool SendData(MessageModel model)
        {
            model.uart_dis_data[] data = new model.uart_dis_data[2];
            data[0].dis_type = (byte)model.LightStatus;
            data[0].ele_id = ((int)model.LightID);
            data[0].little_ele_id = 0;
            data[0].order = (byte)((int)model.LightID % 255) == 0 ? (byte)1 : (byte)((int)model.LightID % 255);
            data[0].port = "COM1";
            data[0].receive_count = 0;
            data[0].send_count = 0;
            data[0].send_count = 100;
            data[0].state = 1;
            return COMPORT_CONTROLLER.write_dis_comment(data);
        }
        /// <summary>
        /// 处理串口数据接收回调函数
        /// </summary>
        public void HandleReceive(model.uart_dis_data[] data)
        {
            if (data.Length > 0)
            {
                foreach (model.uart_dis_data tmp in data)
                {
                    ButtonPressedEvent(MessageModel.ReverseLightID(tmp.ele_id));
                    //回写信息
                }
            }
        }

        /// <summary>
        /// 关闭串口
        /// </summary>
        public void Close()
        {
            COMPORT_CONTROLLER.close_Port();
        }
        private void receive_dis_data_result(byte order, byte result)
        {
            Console.WriteLine("receive_dis_data_result");
        }
        void aa_on_close_channel(model.close_channel_led data)
        {
            Console.WriteLine("关闭通道灯 id = " + data.channel_id.ToString() + " 成功");

        }
        void aa_on_config_channel(model.config_channel_led data)
        {
            Console.WriteLine("配置 id = " + data.channel_id.ToString() + "配置字 = " + data.config_word.ToString() + " 成功 ");
        }
        void aa_on_open_channel(model.open_channel_led data)
        {
            Console.WriteLine("打开通道灯 id = " + data.channel_id.ToString() + " 成功");

        }
    }

}
