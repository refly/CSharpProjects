﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Com.Geek.Beetle.Protocol
{
    public class MessageModel
    {
        public MessageModel(LightID lightID, LightStatus status)
        {
            this.LightID = lightID;
            this.LightStatus = status;
        }
        public LightID LightID { get; set; }
        public LightStatus LightStatus { get; set; }

        public static int ConvertLightID(String strID)
        {
            return 0;
        }

        public static String ReverseLightID(int id)
        {
            switch (id)
            {
                case 1:
                    return "A1";
                case 2:
                    return "A2";
                case 3:
                    return "A3";
                case 4:
                    return "B1";
                case 5:
                    return "B2";
                case 6:
                    return "B3";
                case 7:
                    return "C1";
                case 8:
                    return "C2";
                case 9:
                    return "C3";
                default:
                    return "";
            }
        }
    }
    public enum LightID
    {
        A1 = 1,
        A2 = 2,
        A3 = 3,
        B1 = 4,
        B2 = 5,
        B3 = 6,
        C1 = 7,
        C2 = 8,
        C3 = 9,
        NULL = 0
    }
    public enum LightStatus
    {
        /// <summary>
        /// 灯不亮
        /// </summary>
        OFF = 0,
        /// <summary>
        /// 红
        /// </summary>
        READ = 1,
        /// <summary>
        /// 绿
        /// </summary>
        GREEN = 2,
        /// <summary>
        /// 蓝
        /// </summary>
        BLUE = 3,
        /// <summary>
        /// 红闪
        /// </summary>
        READ_FLASH = 4,
        /// <summary>
        /// 绿闪
        /// </summary>
        GREEN_FLASH = 5,
        /// <summary>
        /// 蓝闪
        /// </summary>
        BLUE_FLASH = 6,
        /// <summary>
        /// 黄
        /// </summary>
        YELLOW = 7,
        /// <summary>
        /// 紫
        /// </summary>
        PURPLE = 8
    }
}
