﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace Com.Geek.Beetle.Config
{
    /// <summary>
    /// 描述：
    ///         系统配置文件读取类,App.config
    /// </summary>
    public class SystemConfig
    {
        /// <summary>
        /// 读取程序默认配置文件，在程序运行期间无法修改
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public static String GetUserSetting(String key)
        {
            return ConfigurationManager.AppSettings[key];
        }
    }

    /// <summary>
    /// 用户自定义配置文件，位置 Resources/Config/SysConfig.xml
    /// </summary>
    public class CustomConfig
    {
        private static String _xmlPath = "Resources/Config/SysConfig.xml";
        private static XmlDocument _doc;
        private static XmlNode _rootNode;

        /// <summary>
        /// 获取工作站ID
        /// </summary>
        /// <returns></returns>
        public static String GetStationID()
        {
            String myid = null;
            XmlNodeList nodeList = GetSysconfigNode().SelectNodes("properties");
            foreach (XmlNode node in nodeList)
            {
                XmlNodeList propertyNodeList = node.SelectNodes("property");
                foreach (XmlNode propertyNode in propertyNodeList)
                {
                    XmlNode nameNode = propertyNode.SelectSingleNode("name");
                    if ("myid".Equals(nameNode.InnerText))
                    {
                        XmlNode valueNode = propertyNode.SelectSingleNode("value");
                        myid = valueNode.InnerText;
                        break;
                    }
                }
            }
            return myid;
        }
        /// <summary>
        /// 获取websocketURL
        /// </summary>
        /// <returns></returns>
        public static String GetWebSocketURL()
        {
            String url = null;
            XmlNodeList nodeList = GetSysconfigNode().SelectNodes("properties");
            foreach (XmlNode node in nodeList)
            {
                XmlNodeList propertyNodeList = node.SelectNodes("property");
                foreach (XmlNode propertyNode in propertyNodeList)
                {
                    XmlNode nameNode = propertyNode.SelectSingleNode("name");
                    if ("webSocketURL".Equals(nameNode.InnerText))
                    {
                        XmlNode valueNode = propertyNode.SelectSingleNode("value");
                        url = valueNode.InnerText;
                        break;
                    }
                }
            }
            return url;
        }
        /// <summary>
        /// 获取com端口的名称
        /// </summary>
        /// <returns></returns>
        public static String GetComPortName()
        {
            String portName = "";
            XmlNodeList nodeList = GetSysconfigNode().SelectNodes("properties");
            foreach (XmlNode node in nodeList)
            {
                XmlNodeList propertyNodeList = node.SelectNodes("property");
                foreach (XmlNode propertyNode in propertyNodeList)
                {
                    XmlNode nameNode = propertyNode.SelectSingleNode("name");
                    if ("COM_PORT".Equals(nameNode.InnerText))
                    {
                        XmlNode valueNode = propertyNode.SelectSingleNode("value");
                        portName = valueNode.InnerText;
                        break;
                    }
                }
            }
            return portName;
        }




        private static XmlNode GetSysconfigNode()
        {
            _doc = new XmlDocument();

            _doc.Load(_xmlPath);

            _rootNode = _doc.SelectSingleNode("configuration");
            return _rootNode.SelectSingleNode("sysconfig");
        }
    }
}
