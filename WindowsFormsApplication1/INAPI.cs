﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Com.GeekPlusBeetle
{
    /**********************************************************
    *描述：    
        页面响应
    ***********************************************************/
    public partial class BeetleMain
    {
        /// <summary>
        /// 播种墙按钮按下回调函数
        /// </summary>
        /// <param name="buttonID"></param>
        public void ButtonPressed(String buttonID)
        {
            MessageBox.Show(buttonID+":被调用了");
            Task<CefSharp.JavascriptResponse> t = this._chromeWebBrowser.GetBrowser().MainFrame.EvaluateScriptAsync("BeetleTrigger.seedingWallButtonPressed(" + buttonID + ")");
        }
    }

}
